
import os
import matplotlib.pyplot as plt
import numpy as np

class DatasetManager():
    def __init__(self):
        return

    def rgb2gray(self,rgb):
        r, g, b = np.rollaxis(rgb[..., :3], axis= -1)
        return 0.2126 * r + 0.7152 * g + 0.0722 * b

    def load_data(self, path, start=None, stop=None, axes = ('b', 0, 1, 'c'), is_color=False):
        topo_view = []
        y = []
        imageCounter = 0
        for fileName in os.listdir(path):
            if fileName.endswith(".jpg") or fileName.endswith(".png"):
                # open with matplotlib
                img = plt.imread(os.path.normpath(os.path.join(path, fileName)))

                # convert to gray-scale
                if is_color == False:
                    img = self.rgb2gray(img)

                # reshape into a single row
                #imgData = imgData.reshape(imgData.shape[0] * imgData.shape[1])

                # append pixel data
                topo_view.append(img)

                # add counter as label
                y.append(imageCounter)

                imageCounter += 1

        topo_view = np.asarray(topo_view, dtype=np.float32)

        y = np.asarray(y)
        y = y.reshape(y.shape[0], 1)

        if start == None:
            start = 0

        if stop == None:
            stop = imageCounter

        print topo_view
        topo_view = topo_view[start:stop, :]
        y = y[start:stop, :]

        # preview the shape
        #print topo_view.shape
        #print y.shape

        # prepare the topo view
        if len(topo_view.shape) == 3: #->m(number of examples), r(row), c(column)
            m, r, c = topo_view.shape
            topo_view = topo_view.reshape(m, r, c, 1)

        #view_converter = DefaultViewConverter(shape=[48,48,1], axes=axes)
        return DenseDesignMatrix(topo_view=topo_view, X=None, y=None, view_converter=None)

if __name__=='__main__':
    datasetDirectory = os.path.join(os.path.dirname('/bigdata/drueda/datasets/dataset_flowers17_1/train1/'))
    dm = DatasetManager()

    myDataset = dm.load_data(datasetDirectory, start=0, stop=2)
    #print myDataset.topo_view.shape