#!/bin/bash
source /etc/profile.d/apps.sh
export PATH="/home/drueda/pylearn2/pylearn2/scripts:$PATH"
export PYLEARN2_DATA_PATH=/home/drueda/pylearn2/pylearn2-datasets
export PYLEARN2_VIEWER_COMMAND="eog --new-instance"
export PATH=/home/drueda/anaconda/bin:$PATH

/home/drueda/anaconda/bin/python /home/drueda/pylearn2/pylearn2-datasets/flowers17/entrenar.py
